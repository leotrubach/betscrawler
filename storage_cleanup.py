import datetime
import shutil
from betscrawler.crawler import storage_path


ARCHIVE_DEPTH = 30


def is_year_dir(d):
    if not d.is_dir():
        return False
    try:
        int(d.name)
    except (ValueError, TypeError):
        return False
    return True


def is_month_dir(d):
    if not d.is_dir():
        return False
    try:
        v = int(d.name)
    except (ValueError, TypeError):
        return False
    if 1 <= v <= 12:
        return True
    return False


def main():
    if not storage_path.exists():
        exit(1)
    now = datetime.datetime.now()
    for year_dir in storage_path.iterdir():
        if not year_dir.is_dir():
            continue
        try:
            year = int(year_dir.name)
        except ValueError:
            continue
        for month_dir in year_dir.iterdir():
            if not month_dir.is_dir():
                continue
            try:
                month = int(month_dir.name)
            except ValueError:
                continue
            for day_dir in month_dir.iterdir():
                if not day_dir.is_dir():
                    continue
                try:
                    day = int(day_dir.name)
                except ValueError:
                    continue
                try:
                    d = datetime.datetime(year, month, day)
                except ValueError:
                    continue
                if now - d > datetime.timedelta(days=ARCHIVE_DEPTH):
                    try:
                        shutil.rmtree(day_dir)
                    except OSError:
                        pass


if __name__ == '__main__':
    main()