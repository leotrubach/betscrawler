import os
import logging
import logging.handlers
from pathlib import Path
from appdirs import AppDirs

appdir = AppDirs('betscrawler', 'leotrubach')
data_dir = Path(appdir.user_data_dir)
log_dir = data_dir / 'logs'
os.makedirs(str(log_dir), exist_ok=True)


class OneLineExceptionFormatter(logging.Formatter):
    def formatException(self, exc_info):
        result = super(OneLineExceptionFormatter, self).formatException(exc_info)
        return repr(result) # or format into one line however you want to

    def format(self, record):
        s = super(OneLineExceptionFormatter, self).format(record)
        if record.exc_text:
            s = s.replace('\n', '') + '|'
        return s


fh = logging.handlers.RotatingFileHandler(str(log_dir / 'crawler.log'), maxBytes=10 * 1024 * 1024)
f = OneLineExceptionFormatter('%(asctime)s %(levelname)s:%(name)s:%(message)s', '%H:%M:%S')
fh.setFormatter(f)
root = logging.getLogger()
root.setLevel(logging.DEBUG)
root.addHandler(fh)


logger = logging.getLogger(__name__)

from betscrawler.crawler import Crawler


def main():
    c = Crawler()
    c.check_for_updates()


if __name__ == '__main__':
    try:
        main()
    except:
        logger.exception('')