import sqlite3
import datetime
import logging
from pathlib import Path

import caribou


logger = logging.getLogger(__name__)
migrations_path = str(Path(__file__).parent / 'migrations')


class DAO(object):
    def __init__(self, db_conn):
        caribou.upgrade(db_conn['DATABASE'], migrations_path)
        self.db = sqlite3.connect(db_conn['DATABASE'])

    def add_record(self, predictor, r):
        row = {'predictor': predictor, **r}
        logger.debug('Adding row to table %s: %s', predictor, r)
        with self.db:
            self.db.execute(
                'INSERT INTO prognosis ('
                '    predictor, progid, proggroup, ts, bet_on, odd, unit, bet, game, added'
                ') '
                'VALUES (:predictor, :progid, :proggroup, :ts, :bet_on, :odd, :unit, :bet, :game, CURRENT_TIMESTAMP)',
                row
            )

    def is_record_present(self, predictor, r):
        c = self.db.cursor()
        c.execute(
            'SELECT * FROM prognosis '
            'WHERE predictor = :predictor '
            'AND game=:game '
            'AND bet_on=:bet_on ',
            {
                'predictor': predictor,
                **r
            })
        return bool(c.fetchone())

    def cleanup(self):
        self.db.execute(
            'DELETE FROM prognosis WHERE added < :expiry', {
                'expiry': datetime.datetime.now() - datetime.timedelta(days=6)
            }
        )
        self.db.commit()