"""
a caribou migration

name: set_date 
version: 20171011023608
"""


def upgrade(connection):
    sql = "UPDATE prognosis SET added = CURRENT_TIMESTAMP"
    connection.execute(sql)
    connection.commit()


def downgrade(connection):
    # add your downgrade step here
    pass
