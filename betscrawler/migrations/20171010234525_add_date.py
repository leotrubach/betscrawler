"""
a caribou migration

name: add_date 
version: 20171010234525
"""


def upgrade(connection):
    sqls = [
        'CREATE TABLE IF NOT EXISTS prognosis ('
        'predictor text, progid text, proggroup text, '
        'ts text, bet_on text, odd text, unit text, '
        'bet text, game text)',
        "ALTER TABLE prognosis ADD COLUMN added TIMESTAMP"
    ]
    for sql in sqls:
        connection.execute(sql)
    connection.commit()


def downgrade(connection):
    # add your downgrade step here
    pass
