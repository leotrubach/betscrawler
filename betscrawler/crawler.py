import datetime
import json
import lzma
import logging
import re
import os
from pathlib import Path
import time
from typing import Pattern

logger = logging.getLogger(__name__)

import requests
from lxml import etree
from html import unescape
from lxml.html import HTMLParser
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from telegram.bot import Bot
from appdirs import AppDirs

appdir = AppDirs('betscrawler', 'leotrubach')
data_dir = Path(appdir.user_data_dir)

from betscrawler.settings import COOKIES_DB, DAO_DB, FIREFOX_BINARY, BOT_TOKEN, CHAT_ID, HEADLESS_FIREFOX
from .dao import DAO

db_path = str(data_dir / DAO_DB)
storage_path = data_dir / 'storage'


def totxt(el, sep=' '):
    if el is None:
        return ''
    text = list(filter(None, [unescape(a).strip() for a in el.itertext()]))
    return sep.join(text)


bet_rxp = re.compile('(?P<team_name>\w+):\b(?P<tail>.*)')

team_rxp = re.compile('.*?\(.*?\)\s*(?P<team1>.+?) - (?P<team2>.+?)\s*\(.*?\)')


def strip_team(row):
    try:
        bet = row['bet_on']
        team = row['game']
    except KeyError:
        return row
    mo = team_rxp.match(team)
    if not mo:
        return row
    bet = bet.replace(mo.group('team1'), '').replace(mo.group('team2'), '').replace(':', '').strip()
    rv = row.copy()
    rv['bet_on'] = bet
    return rv


SUBSTITUTE = (
    (re.compile('Bet on Spread (?P<value>.+)'), 'Bet on \g<value> PointSpread'),
    ('Bet on ML', 'Bet on Moneyline')
)


def normalize_bet(row):
    rv = row.copy()
    for f, t in SUBSTITUTE:
        if isinstance(f, Pattern):
            if f.match(rv['bet_on']):
                rv['bet_on'] = f.sub(t, rv['bet_on'])
                return rv
        elif isinstance(f, str):
            if rv['bet_on'] == f:
                rv['bet_on'] = t
                return rv
    return row



class CookiesObsolete(Exception):
    pass


class Crawler(object):
    login_link = (
        'http://zcodesystem.com/zcodebacktestingtools_cb.php?item=18&cbreceipt=8ZPKAKC6&'
        'time=1499174816&cbpop=1DFEE675&cbaffi=0&cname=Kuan+Balabaev&cemail=ubisoft.com%40mail.ru&'
        'ccountry=RU&czip=&cbskin=11923')

    links = [
        ('razputin', 'http://zcodesystem.com/vipclub/razputinsystem.php'),
        ('wadewilson', 'http://zcodesystem.com/vipclub/wadewilsonsystem.php'),
        ('edis', 'http://zcodesystem.com/vipclub/edis_soccer_system.php'),

    ]
    
    keywords = {
        'razputin': (
            re.compile(r'.*\bsoccer selective s-bomb\b.*'),
        ),
        'wadewilson': (
            re.compile(r'.*\bsoccer spf 2.0\b.*'),
        ),
        'edis': (
            re.compile(r'.*\bsoccer 10u easymoney\b.*'),
        )
    }

    def __init__(self):
        self.cookies = {}
        self.bot = Bot(BOT_TOKEN)
        os.makedirs(str(data_dir), exist_ok=True)
        self.last_sent = None
        self._load()

    def _load(self):
        try:
            with (data_dir / COOKIES_DB).open() as f:
                self.cookies = json.load(f)
        except (IOError, ValueError):
            pass
        self.session = requests.Session()
        self.session.headers.update({
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                          '(KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
        })
        self.session.cookies.update(self.cookies)
        self.parser = HTMLParser()
        self.dao = DAO({"DATABASE": db_path})

    def wait_second_page_link(self, driver):
        try:
            second_link = driver.find_element_by_xpath('//a[@href="/zcode_thanks_cb.php"]')
        except NoSuchElementException:
            return False
        return second_link

    def get_cookies(self):
        log_dir = data_dir / 'logs'
        firefox_log_file = (log_dir/ 'firefox.log').open('a+')
        gecko_log = log_dir / 'geckodriver.log'
        binary = FirefoxBinary(FIREFOX_BINARY, log_file=firefox_log_file)
        if HEADLESS_FIREFOX:
            from pyvirtualdisplay import Display
            display = Display(visible=0, size=(800, 600))
            display.start()

            binary.add_command_line_options('-headless')
        driver = webdriver.Firefox(firefox_binary=binary, log_path=str(gecko_log))
        driver.get(self.login_link)
        first_link = driver.find_element_by_xpath('//a[@href="/antivegas"]')
        first_link.click()

        EC.presence_of_element_located((By.LINK_TEXT, '/zcode_thanks_cb.php'))
        second_link = WebDriverWait(driver, 5).until(self.wait_second_page_link)
        second_link.click()
        r = {}
        for c in driver.get_cookies():
            if c['domain'].endswith('zcodesystem.com'):
                r[c['name']] = c['value']
        logger.debug('Got cookies: %s', r)
        driver.close()
        return r

    def fetch_data(self):
        results = {}
        d = datetime.datetime.now()
        sub_dir = storage_path / str(d.year) / str(d.month) / str(d.day) / str(d.hour) / str(d.minute)
        for linkname, l in self.links:
            logging.info('Loading %s...', linkname)
            resp = self.session.get(l)
            if 'empty session' in resp.content.decode(errors='ignore').lower():
                raise CookiesObsolete()
            sub_dir.mkdir(parents=True, exist_ok=True)
            with lzma.open(str(sub_dir / linkname) + '.html.xz', 'w') as f:
                f.write(resp.content)
            parsed = etree.fromstring(resp.content, parser=self.parser)
            link_result = []
            last_good = False
            group = None
            for row in parsed.xpath('//div[contains(@class,"typPerformanceA")]/table/tr'):
                children = row.xpath('./td[not(contains(@style,"display:none"))]')
                if len(children) == 8:
                    if not last_good:
                        logger.warning('Got 8 element row but previous one was not good: %s', etree.tostring(row))
                        continue
                    rowid, ts, _, bet_on, odd, unit, bet, game = children
                    logger.info('Got 8 element row %s', etree.tostring(row))
                    last_good = True
                elif len(children) == 5:
                    if not last_good:
                        logger.warning('Got 5 element row but previous one was not good: %s', etree.tostring(row))
                        continue
                    bet_on, odd, unit, bet, game = children
                    rowid, ts = None, None
                    last_good = True
                elif len(children) == 6:
                    group, bet_on, odd, unit, bet, game = children
                    rowid, ts = None, None
                    last_good = True
                elif len(children) == 9:
                    rowid, group, ts, _, bet_on, odd, unit, bet, game = children
                    last_good = True
                else:
                    logging.warning('Got bad row: %s', etree.tostring(row))
                    last_good = False
                    continue
                result = {
                    'progid': totxt(rowid, ''),
                    'proggroup': totxt(group, ' - '),
                    'ts': totxt(ts, ''),
                    'bet_on': totxt(bet_on, ''),
                    'odd': totxt(odd),
                    'unit': totxt(unit),
                    'bet': totxt(bet),
                    'game': totxt(game)
                }
                if self.is_result_suitable(result, self.keywords[linkname]):
                    link_result.append(result)
            results[linkname] = link_result
        return results

    @staticmethod
    def is_result_suitable(result, keywords) -> bool:
        group = result['proggroup'].lower()
        for k in keywords:
            if isinstance(k, str):
                if k in group:
                    return True
            if isinstance(k, Pattern):
                if k.match(group):
                    return True
        return False

    def notify(self, predictor, row):
        text = "{predictor}\n{ts}\n{proggroup}\n{bet_on}\n{odd}\n{unit} = {bet}\n{game}".format(
            predictor=predictor, **row)
        cur_time = time.monotonic()
        if self.last_sent is not None:
            delta = cur_time - self.last_sent
            if delta < 5:
                time.sleep(5 - delta)
        self.bot.send_message(CHAT_ID, text)
        self.last_sent = cur_time

    def check_for_updates(self):
        try:
            results = self.fetch_data()
        except CookiesObsolete:
            logger.warning('Cookies seem to be obsolete. Trying to get new ones')
            try:
                self.bot.send_message(CHAT_ID, 'Cookies are obsolete. Updating cookies')
            except:
                pass
            cookies = self.get_cookies()
            with (data_dir / COOKIES_DB).open('w') as f:
                json.dump(cookies, f)
            self._load()
            results = self.fetch_data()

        for predictor, rows in results.items():
            for row in rows:
                nr = normalize_bet(strip_team(row))
                if not self.dao.is_record_present(predictor, nr):
                    self.notify(predictor, row)
                    self.dao.add_record(predictor, nr)
                else:
                    logger.debug('Record already present %s: %s', predictor, row)
