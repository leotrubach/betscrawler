from betscrawler.dao import DAO
from betscrawler.crawler import db_path


def main():
    d = DAO({'DATABASE': db_path})
    d.cleanup()


if __name__ == '__main__':
    main()